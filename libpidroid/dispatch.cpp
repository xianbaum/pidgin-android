#include "pidroid.h"

const uint32_t NULL_TYPE_ID = 0; // from Uri.java
const uint32_t RESULT_OK = -1; // from Activity.java
const uint32_t BROADCAST_INTENT_TRANSACTION =
        IBinder::FIRST_CALL_TRANSACTION + 13;
String16 IACTIVITYMANAGER("android.app.IActivityManager");
// Keep in sync with app/src/org/pidroid/PidroidConstants.java
String16 REQUIRED_PERMISSION("org.pidroid.permission.PIDROID_SERVICE");


static gboolean eventPrepare(GSource* source, gint* timeout) {
    LOGD("[eventPrepare]");
    *timeout = -1;
    return FALSE;
}


static gboolean eventCheck(GSource* source) {
    LOGD("[eventCheck]");
    EventSource* eventSource = (EventSource*)source;
    return eventSource->pollFd.revents & G_IO_IN;
}


static gboolean eventDispatch(GSource* source, GSourceFunc callback, gpointer) {
    LOGD("[eventDispatch]");
    EventSource* eventSource = (EventSource*)source;
    // TODO: read parcel(s)

    jint eventId;
    if (read(gEventPipe[0], &eventId, sizeof(eventId)) != sizeof(eventId)) {
        LOGE("[eventDispatch]: read failed");
        return FALSE;
    }

    switch (eventId) {
    case EVENT_QUIT_PURPLE:
        return quitPurple(gEventPipe[0]);
    }

    return TRUE;
}


GSourceFuncs gEventFuncs = {
    eventPrepare,
    eventCheck,
    eventDispatch,
    NULL
};


jboolean sendEvent(JNIEnv* env, jobject self, jint eventId) {
    LOGD("[sendEvent]: %d", eventId);

    if (write(gEventPipe[1], &eventId, sizeof(eventId)) != sizeof(eventId)) {
        LOGE("[sendEvent]: write failed");
        return FALSE;
    }

    return TRUE;
}


void startBroadcastIntent(Parcel& data, const String16& action) {
    data.writeInterfaceToken(IACTIVITYMANAGER);
    data.writeStrongBinder(NULL); // caller
    // intent
        data.writeString16(action); // Intent.action
        data.writeInt32(NULL_TYPE_ID); // Intent.data
        data.writeString16(NULL, 0); // Intent.type
        data.writeInt32(0); // writeToParcel flags
        data.writeString16(NULL, 0); // component name
        data.writeInt32(0); // size of Intent.categories
        // Intent extras ...
}


int32_t endBroadcastIntent(Parcel& data) {

        // ... Intent extras
    data.writeString16(NULL, 0); // resolvedType
    data.writeStrongBinder(NULL); // resultTo
    data.writeInt32(RESULT_OK); // resultCode
    data.writeString16(NULL, 0); // resultData
    data.writeInt32(-1); // map
    data.writeString16(REQUIRED_PERMISSION); // requiredPermission
    data.writeInt32(0); // serialized
    data.writeInt32(0); // sticky

    Parcel reply;
    status_t ret = gAm->transact(BROADCAST_INTENT_TRANSACTION, data, &reply);
    if (ret != NO_ERROR) {
        LOGE("[endBroadcastIntent]: transact failed: %d", ret);
        return -1;
    }
    reply.readInt32(); // exceptionCode (always 0)
    return reply.readInt32();
}
