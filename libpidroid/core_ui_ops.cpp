#include "pidroid.h"

static void pidroid_ui_init(void) {
    LOGD("[pidroid_ui_init]");
}

PurpleCoreUiOps pidroid_core_ui_ops = {
    NULL,               // ui_prefs_init
    NULL,               // debug_ui_init
    pidroid_ui_init,    // ui_init
    NULL,               // quit
    NULL,               // get_ui_info
    NULL,               // _purple_reserved1
    NULL,               // _purple_reserved2
    NULL                // _purple_reserved3
};
