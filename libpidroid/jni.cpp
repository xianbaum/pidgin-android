#include "pidroid.h"

JavaVM* gJvm;


const static JNINativeMethod gJniMethods[] = {
    { "createPurple", "()Z", (void *)createPurple },
    { "destroyPurple", "()Z", (void *)destroyPurple },
//    { "sendEvent", "()Z", (void*)sendEvent }
};


JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* jvm, void* reserved) {
    LOGD("[JNI_OnLoad]");

    JNIEnv* env;
    if (jvm->GetEnv((void**)&env, JNI_VERSION_1_2)) {
        LOGE("[JNI_OnLoad]: Unable to get JNI environment");
        return JNI_ERR;
    }

    jclass cls = env->FindClass("org/pidroid/service/PidroidService");
    if (cls == NULL) {
        LOGE("[JNI_OnLoad]: Unable to find class");
        return JNI_ERR;
    }
    if (env->RegisterNatives(cls, gJniMethods, sizeof(gJniMethods) / sizeof(gJniMethods[0])) < 0) {
        LOGE("[JNI_OnLoad]: Unable to register native methods");
        return JNI_ERR;
    }

    gJvm = jvm;

    return JNI_VERSION_1_2;
}

// TODO: JNI_OnUnload ?
