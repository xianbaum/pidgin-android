#include "pidroid.h"

// Keep in sync with app/src/org/pidroid/PidroidConstants.java
String16 CREATE_PURPLE_SUCCEEDED("org.pidroid.intent.action.CREATE_PURPLE_SUCCEEDED");
String16 CREATE_PURPLE_FAILED("org.pidroid.intent.action.CREATE_PURPLE_FAILED");

sp<IBinder> gAm = NULL;
static GMainLoop* gLoop = NULL;
static pthread_t gThread;
int gEventPipe[2];
guint gSourceId;


static void* glibThread(void *arg) {
    LOGD("[glibThread]");
    Parcel data;

    ThreadArg* threadArg = (ThreadArg*)arg;
    JNIEnv* env;
    gJvm->AttachCurrentThread(&env, NULL);
    threadArg->setEnv(env);
    jclass protocolCls = threadArg->protocolCls;
    jfieldID mProtocolsFld = env->GetFieldID(threadArg->pidroidServiceCls,
        "mProtocols", "[Lorg/pidroid/Protocol;");
    jmethodID protocolCtor = env->GetMethodID(protocolCls, "<init>",
        "(Ljava/lang/String;Ljava/lang/String;)V");
    jobjectArray protocols = NULL;
    GList* iter = NULL;
    GSource* source = NULL;
    EventSource* eventSource;

    // TODO: check returns

    // init plugins
    purple_init_ssl_plugin();
    purple_init_ssl_openssl_plugin();
    purple_init_jabber_plugin();
    purple_init_msn_plugin();
    purple_init_aim_plugin();
    purple_init_icq_plugin();
    purple_init_yahoo_plugin();
    purple_init_facebook_plugin();

    purple_core_set_ui_ops(&pidroid_core_ui_ops);
    purple_eventloop_set_ui_ops(&pidroid_eventloop_ui_ops);
    if (!purple_core_init(UI_ID)) {
        LOGE("[glibThread]: purple_core_init failed");
        goto error;
    }
    purple_set_blist(purple_blist_new());
    purple_blist_load();
    purple_prefs_load();
    purple_pounces_load();

    iter = purple_plugins_get_protocols();
    protocols = env->NewObjectArray(g_list_length(iter), protocolCls, NULL);
    if (!protocols) {
        LOGE("[glibThread]: env->NewObjectArray failed");
        goto error;
    }

    for (jsize i = 0; iter; iter = iter->next, ++i) {
        PurplePlugin* plugin = (PurplePlugin*)iter->data;
        PurplePluginInfo* info = (PurplePluginInfo*)plugin->info;
        env->SetObjectArrayElement(protocols, i, env->NewObject(protocolCls,
            protocolCtor, env->NewStringUTF(info->name), env->NewStringUTF(info->id)));
    }
    env->SetObjectField(threadArg->pidroidService, mProtocolsFld, protocols);

    if (pipe(gEventPipe) != 0) {
        LOGE("[glibThread]: pipe failed");
        g_source_unref(source);
        goto error;
    }
    source = g_source_new(&gEventFuncs, sizeof(EventSource));
    eventSource = (EventSource*)source;
    eventSource->pollFd.fd = gEventPipe[0];
    eventSource->pollFd.events = G_IO_IN | IO_ERRORS;
    g_source_add_poll(source, &eventSource->pollFd);
    gSourceId = g_source_attach(source, NULL);
    g_source_unref(source);

    startBroadcastIntent(data, CREATE_PURPLE_SUCCEEDED);
    data.writeInt32(-1); // empty bundle
    endBroadcastIntent(data);

    gLoop = g_main_loop_new(NULL, FALSE);
    g_main_loop_run(gLoop);
    purple_core_quit();

    LOGD("[glibThread]: Done glib loop");
    delete threadArg;
    gJvm->DetachCurrentThread();
    return NULL;

error:
    purple_core_quit();

    startBroadcastIntent(data, CREATE_PURPLE_FAILED);
    data.writeInt32(-1); // empty bundle
    endBroadcastIntent(data);

    delete threadArg;
    gJvm->DetachCurrentThread();
    return NULL;
}


jboolean createPurple(JNIEnv* env, jobject self) {
    LOGD("[createPurple]");

    if (gLoop != NULL) {
        LOGE("[createPurple]: glib thread already created");
        return JNI_FALSE;
    }

    if (gAm == NULL) {
        sp<IServiceManager> sm = defaultServiceManager();
        // XXX: can gAm ever become invalid?
        gAm = sm->getService(String16("activity"));
    }

    ThreadArg* threadArg = new ThreadArg(env, self);
    if (pthread_create(&gThread, NULL, glibThread, threadArg) != 0) {
        LOGE("[createPurple]: pthread_create failed");
        return JNI_FALSE;
    }

    return JNI_TRUE;
}


jboolean quitPurple(int fd) {
    LOGD("[quitPurple]");
    purple_core_quit();
    g_main_loop_quit(gLoop);
    return TRUE;
}


jboolean destroyPurple(JNIEnv* env, jobject self) {
    LOGD("[destroyPurple]");

    if (gLoop == NULL) {
        LOGE("[destroyPurple]: glib thread not created");
        return JNI_FALSE;
    }

    if (!sendEvent(env, self, EVENT_QUIT_PURPLE)) {
        return JNI_FALSE;
    }

    void *value;
    pthread_join(gThread, &value);
    gLoop = NULL;

    return JNI_TRUE;
}
