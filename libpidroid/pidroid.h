#define LOG_TAG "PidroidJni"
#define UI_ID "Pidroid"
#define IO_ERRORS (G_IO_HUP | G_IO_ERR | G_IO_NVAL)

#include <pthread.h>
#include <glib.h>
#include <purple.h>

#include <nativehelper/jni.h>
#include <utils/IBinder.h>
#include <utils/IServiceManager.h>
#include <utils/Parcel.h>
#include <utils/String8.h>
#include <cutils/atomic.h>
#include <cutils/log.h>

using namespace android;


struct ThreadArg {
    ThreadArg(JNIEnv* env, jobject self) : env(env) {
        jclass pidroidServiceCls = env->GetObjectClass(self);
        jclass protocolCls = env->FindClass("org/pidroid/Protocol");

        this->_pidroidService = self;
        this->_pidroidServiceCls = pidroidServiceCls;
        this->_protocolCls = protocolCls;

        this->pidroidService = env->NewGlobalRef(self);
        this->pidroidServiceCls = (jclass)env->NewGlobalRef((jobject)pidroidServiceCls);
        this->protocolCls = (jclass)env->NewGlobalRef((jobject)protocolCls);
    }

    inline void setEnv(JNIEnv* env) {
        this->env = env;
    }

    ~ThreadArg() {
        env->DeleteGlobalRef(_pidroidService);
        env->DeleteGlobalRef((jobject)_pidroidServiceCls);
        env->DeleteGlobalRef((jobject)_protocolCls);
    }

    jobject pidroidService;
    jclass pidroidServiceCls;
    jclass protocolCls;

private:
    jobject _pidroidService;
    jclass _pidroidServiceCls;
    jclass _protocolCls;
    JNIEnv* env;
};

extern PurpleCoreUiOps pidroid_core_ui_ops;
extern PurpleEventLoopUiOps pidroid_eventloop_ui_ops;

struct InputAddData {
    PurpleInputFunction function;
    gpointer user_data;
};

struct EventSource {
  GSource source;
  GPollFD pollFd;
};

enum EventId {
    EVENT_QUIT_PURPLE = 1,
};

extern "C" {
    extern gboolean purple_init_ssl_plugin(void);
    extern gboolean purple_init_ssl_openssl_plugin(void);
    extern gboolean purple_init_jabber_plugin(void);
    extern gboolean purple_init_msn_plugin(void);
    extern gboolean purple_init_aim_plugin(void);
    extern gboolean purple_init_icq_plugin(void);
    extern gboolean purple_init_yahoo_plugin(void);
    extern gboolean purple_init_facebook_plugin(void);
}

extern jboolean createPurple(JNIEnv* env, jobject self);
extern jboolean quitPurple(int fd);
extern jboolean destroyPurple(JNIEnv* env, jobject self);
extern GSourceFuncs gEventFuncs;
extern jboolean sendEvent(JNIEnv* env, jobject self, jint eventId);
extern void startBroadcastIntent(Parcel& data, const String16& action);
extern int32_t endBroadcastIntent(Parcel& data);

extern JavaVM* gJvm;
extern sp<IBinder> gAm;
extern int gEventPipe[2];

extern String16 REQUIRED_PERMISSION;
extern String16 CREATE_PURPLE_SUCCEEDED;
extern String16 CREATE_PURPLE_FAILED;
