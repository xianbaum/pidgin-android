#include "pidroid.h"


static void pidroid_destroy_data(gpointer data) {
    g_free(data);
}


static gboolean pidroid_io_func(GIOChannel *source,
        GIOCondition condition, gpointer data) {
    InputAddData* cb_data = (InputAddData*)data;

    PurpleInputCondition cond = (PurpleInputCondition)0;
    if (condition & (G_IO_IN | IO_ERRORS))
        cond = (PurpleInputCondition)(cond | PURPLE_INPUT_READ);
    if (condition & (G_IO_OUT | IO_ERRORS))
        cond = (PurpleInputCondition)(cond | PURPLE_INPUT_WRITE);

    cb_data->function(cb_data->user_data, g_io_channel_unix_get_fd(source), cond);

    return TRUE;
}


static guint pidroid_input_add(gint fd, PurpleInputCondition cond,
        PurpleInputFunction function, gpointer user_data) {
    LOGD("[pidroid_input_add]");
    GIOChannel* channel = NULL;
    InputAddData* cb_data = NULL;
    guint result = 0;
    GIOCondition glib_cond;

    channel = g_io_channel_unix_new(fd);
    if (!channel)
        goto end;

    cb_data = (InputAddData*)g_malloc(sizeof(InputAddData));
    if (!cb_data)
        goto end;
    cb_data->function = function;
    cb_data->user_data = user_data;

    glib_cond = (GIOCondition)0;
    if (cond & PURPLE_INPUT_READ)
        glib_cond = (GIOCondition)(glib_cond | (G_IO_IN | IO_ERRORS));
    if (cond & PURPLE_INPUT_WRITE)
        glib_cond = (GIOCondition)(glib_cond | (G_IO_OUT | IO_ERRORS));

    result = g_io_add_watch_full(channel, G_PRIORITY_DEFAULT, glib_cond,
            pidroid_io_func, cb_data, pidroid_destroy_data);

end:
    g_io_channel_unref(channel);
    return result;
}


PurpleEventLoopUiOps pidroid_eventloop_ui_ops = {
    g_timeout_add,              // timeout_add
    g_source_remove,            // timeout_remove
    /* We can't use g_io_add_watch because the signature doesn't match. */
    pidroid_input_add,          // input_add
    g_source_remove,            // input_remove
    NULL,                       // input_get_error
    g_timeout_add_seconds,      // timeout_add_seconds
    NULL,                       // _purple_reserved2
    NULL,                       // _purple_reserved3
    NULL                        // _purple_reserved4
};
