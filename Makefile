export TARGET = arm-none-linux-gnueabi
export CC = /opt/android-ndk/toolchains/llvm/prebuilt/linux-x86_64/bin/clang
export PREFIX = /tmp/org.pidroid
export JOBS = -j8
export DROID = $(shell echo `which arm-eabi-gcc` | sed 's/^\(.*\)\/prebuilt\/.*/\1/')

SUBDIRS1 = libiconv \
           libintl

SUBDIRS2 = libtarxz \
           libpidroid

# We need bash to deal with {foo,bar} glob
# TODO: this won't work if we run make for the first time (the .so files aren't installed)
TARLIST = $(shell bash -c "echo `sed '/^$$/d' tarlist | \
    sed 's:^:$(PREFIX)/:' | tr '\n' ' ' `" | \
    sed 's:$(PREFIX)/::g')

all: debug

assets/pidroid.tar.xz:
	install -d $(PREFIX)/include
	install include/*.h $(PREFIX)/include
	for subdir in $(SUBDIRS1); do \
	  (cd $$subdir; make;) || exit 1; \
	done
	./build_third_party.sh
	for subdir in $(SUBDIRS2); do \
	  (cd $$subdir; make;) || exit 1; \
	done
	for i in $(TARLIST); do \
#	  $(TARGET)-strip -s -x $(PREFIX)/$$i 2> /dev/null || true; \
	done
	tar cf assets/pidroid.tar -C $(PREFIX) $(TARLIST)
	$(PREFIX)/native/bin/xz --lzma2=dict=1048576 -Ccrc32 -f assets/pidroid.tar

debug: assets/pidroid.tar.xz
	(cd app; ant debug)

release: assets/pidroid.tar.xz
	(cd app; ant debug)

clean:
	for subdir in app $(SUBDIRS1) $(SUBDIRS2); do \
	  (cd $$subdir; make clean;) || exit 1; \
	done
	./build_third_party.sh clean
	rm -rf assets $(PREFIX)

install: debug
	adb -e install -r app/bin/Pidroid-debug.apk
	adb -e logcat

.PHONY: debug release assets/pidroid.tar.xz
