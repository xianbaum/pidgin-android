extern "C" {
    #include "xz/xz.h"
}
#include <libtar.h>
#include <fcntl.h>

#include <nativehelper/jni.h>
#include <utils/Asset.h>
#include <utils/AssetManager.h>
#include <utils/String8.h>
#include <cutils/log.h>

#ifndef MIN
# define MIN(a,b) ((a<b)?a:b)
#endif

using namespace android;

static const int BUFFER_SIZE = 128 * 1024;
static const int DICT_SIZE = 1024 * 1024;
static const int HALF_SIZE = BUFFER_SIZE >> 1;

struct State {

    xz_dec* xzDec;
    xz_buf xzBuf;
    size_t xzInOffset; // The offset to the end of the unconsumed input
    size_t xzOutOffset; // The offset to the start of unconsumed output
    uint8_t xzInBuf[BUFFER_SIZE];
    uint8_t xzOutBuf[BUFFER_SIZE];
    bool xzEof;

    Asset* asset;
    bool assetEof;
};

// TODO: Use non-global state
State gState;

struct AssetFile {
    AssetManager assetManager;
    const char* assetName;
};


int assetOpen(const char* file, int flags, ...) {
    State* state = &gState;

    memset(state, 0, sizeof(State));
    state->xzBuf.in = state->xzInBuf;
    state->xzBuf.in_size = BUFFER_SIZE;
    state->xzBuf.out = state->xzOutBuf;
    state->xzBuf.out_size = BUFFER_SIZE;

    AssetFile* assetFile = (AssetFile*)file;

    state->asset = assetFile->assetManager.open(assetFile->assetName, Asset::ACCESS_STREAMING);
    if (!state->asset) {
        LOGE("[assetOpen]: assetManager.open failed");
        return -1;
    }

    state->xzDec = xz_dec_init(DICT_SIZE);
    if (!state->xzDec) {
        state->asset->close();
        LOGE("[assetOpen]: xz_dec_init failed");
    }
}


ssize_t assetRead(int fd, void* data, size_t size) {
    State* state = &gState;
    ssize_t origSize = size;

    if (size == 0) {
        return 0;
    }

    do {
        // If we have output data from a previous call use that first
        size_t avail = state->xzBuf.out_pos - state->xzOutOffset;
        if (avail > 0) {
            size_t copySize = MIN(avail, size);
            memcpy (data, state->xzBuf.out + state->xzOutOffset, copySize);
            data = (char*)data + copySize;
            size -= copySize;
            state->xzOutOffset += copySize;

            // If the lower half of the output buffer is empty shift the top half down
            if (state->xzOutOffset > HALF_SIZE) {
                memcpy(state->xzBuf.out, state->xzBuf.out + state->xzOutOffset,
                    state->xzBuf.out_pos - state->xzOutOffset);
                state->xzBuf.out_pos -= state->xzOutOffset;
                state->xzOutOffset = 0;
            }
        }

        // If the lower half of the input buffer is empty shift the top half down
        if (state->xzBuf.in_pos > HALF_SIZE) {
            size_t inLeft = state->xzInOffset - state->xzBuf.in_pos;
            memcpy((void*)state->xzBuf.in, state->xzBuf.in + state->xzBuf.in_pos, inLeft);
            state->xzInOffset = inLeft;
            state->xzBuf.in_pos = 0;
        }

        // The input buffer must always be full
        while (!state->assetEof && state->xzInOffset < state->xzBuf.in_size) {
            ssize_t assetRet = state->asset->read((void*)(state->xzBuf.in + state->xzInOffset),
                state->xzBuf.in_size - state->xzInOffset);
            if (assetRet == -1) {
                return -1;
            } else if (assetRet == 0) {
                state->assetEof = true;
            } else {
                state->xzInOffset += assetRet;
            }
        }

        // Decompress until either the input buffer is empty or the output buffer is full
        while (!state->xzEof && (state->xzBuf.in_pos < state->xzBuf.in_size) &&
                    (state->xzBuf.out_pos < state->xzBuf.out_size)) {
            xz_ret xzRet = xz_dec_run(state->xzDec, &state->xzBuf);
            if (xzRet == XZ_STREAM_END) {
                state->xzEof = true;
            } else if (xzRet != XZ_OK) {
                // XZ_*_ERROR
                return -1;
            }
        }
    }
    while (size > 0 && (state->xzBuf.out_pos - state->xzOutOffset) > 0) ;

    if (state->xzEof && (state->xzInOffset - state->xzBuf.in_pos) != 0) {
        LOGD("[assetRead]: still have asset data but hit xz EOF");
    }

    return origSize - size;
}


int assetClose(int fd) {
    State* state = &gState;

    state->asset->close();
    xz_dec_end(state->xzDec);

    return 0;
}


jboolean extractAsset(JNIEnv* env, jclass cls, jstring sourceDir,
        jstring assetName, jstring purpleDir) {

    AssetFile assetFile;

    const char* sourceDirC = env->GetStringUTFChars(sourceDir, NULL);
    if (!sourceDirC) {
        LOGE("[extractAsset]: GetStringUTFChars(sourceDir) failed");
        return JNI_FALSE;
    }
    String8 sourceDir8(sourceDirC);
    env->ReleaseStringUTFChars(sourceDir, sourceDirC);

    void* cookie;
    if (!assetFile.assetManager.addAssetPath(sourceDir8, &cookie)) {
        LOGE("[extractAsset]: addAssetPath failed");
        return JNI_FALSE;
    }

    assetFile.assetName = env->GetStringUTFChars(assetName, NULL);
    if (!assetFile.assetName) {
        LOGE("[extractAsset]: GetStringUTFChars(assetName) failed");
        return JNI_FALSE;
    }

    tartype_t tarType;
    tarType.openfunc = assetOpen;
    tarType.readfunc = assetRead;
    tarType.writefunc = NULL;
    tarType.closefunc = assetClose;
    TAR* tar;
    int fd = tar_open(&tar, (char*)&assetFile, &tarType, O_RDONLY, 0, 0);
    env->ReleaseStringUTFChars(assetName, assetFile.assetName);
    if (fd == -1) {
        LOGE("[extractAsset]: tar_open failed");
        return JNI_FALSE;
    }

    const char* purpleDirC = env->GetStringUTFChars(purpleDir, NULL);
    if (!purpleDirC) {
        LOGE("[extractAsset]: GetStringUTFChars(assetName) failed");
        tar_close(tar);
        return JNI_FALSE;
    }
    int ret = tar_extract_all(tar, (char*)purpleDirC);

    tar_close(tar);

    if (ret != 0) {
        LOGE("[extractAsset]: failed to read tar file (%d | %s)", ret, purpleDirC);
        return JNI_FALSE;
    }
    env->ReleaseStringUTFChars(purpleDir, purpleDirC);

    return JNI_TRUE;
}


const static JNINativeMethod gJniMethods[] = {
    { "extractAsset", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z",
        (void *)extractAsset }
};


JNIEXPORT jint JNICALL JNI_OnLoad(JavaVM* jvm, void* reserved) {
    JNIEnv* env;
    if (jvm->GetEnv((void**)&env, JNI_VERSION_1_2)) {
        LOGE("[JNI_OnLoad]: Unable to get JNI environment");
        return JNI_ERR;
    }

    jclass cls = env->FindClass("org/pidroid/service/PidroidService");
    if (cls == NULL) {
        LOGE("[JNI_OnLoad]: Unable to find class");
        return JNI_ERR;
    }
    if (env->RegisterNatives(cls, gJniMethods, sizeof(gJniMethods) / sizeof(gJniMethods[0])) < 0) {
        LOGE("[JNI_OnLoad]: Unable to register native methods");
        return JNI_ERR;
    }

    xz_crc32_init();

    return JNI_VERSION_1_2;
}
