#!/bin/sh

set -e

arch=$1
api=$2
toolchain_path=/opt/android-toolchain-$arch-$api
prefix_path=/opt/android-$arch-$api

target_host=aarch64-linux-android
export AR=$target_host-ar
export AS=$target_host-clang
export CC=$target_host-clang
export CXX=$target_host-clang++
export LD=$target_host-ld
export STRIP=$target_host-strip
export PATH=$PATH:$toolchain_path/bin
export PREFIX=/home/user/androidlib/
export TARGET=aarch64-unknown-linux-gnu
CROSS_BUILD="--host=$TARGET --target=$TARGET --prefix=$PREFIX"
NATIVE_BUILD="--target=$TARGET --prefix=$PREFIX/native"
export CFLAGS="-I$PREFIX/include -I$DROID/external/icu4c/common -O2 -I/home/user/androidlib/include"

export LDFLAGS="-L$PREFIX/lib -Wl,-rpath-link=$PREFIX/lib -L/home/user/androidlib/lib64/"
export CC="$toolchain_path/bin/$CC"
export CXX="$toolchain_path/bin/$CXX"
export AR="$toolchain_path/bin/$AR"
export LD="$toolchain_path/bin/$LD"
export STRIP="$toolchain_path/bin/$STRIP"
export PKGCONFIG="$PKG_CONFIG"
export PKG_CONFIG_PATH='/home/usr/androidlib/lib64/pkgconfig/'
export GLIB_CFLAGS='-I/home/user/androidlib/include/glib-2.0 -I/home/user/androidlib/lib64/glib-2.0/include/'
export GLIB_LIBS='-L/home/user/androidlib/lib64/glib-2.0'
set -ex

BINUTILS="binutils-2.17|\
.tar.bz2|\
http://ftp.gnu.org/pub/gnu/binutils/|\
NATIVE|\
--disable-werror --enable-install-libbfd --enable-64-bit-bfd=no"

GLIB="glib-2.22.4|\
.tar.bz2|\
http://ftp.gnome.org/pub/gnome/sources/glib/2.22/|\
AUTOGEN_SUBDIR_MODE=1 ./autogen.sh; \
    rm glib/galiasdef.c gobject/gobjectaliasdef.c; \
    ac_cv_func_posix_getgrgid_r=no ac_cv_func_posix_getpwuid_r=no \
    glib_cv_uscore=no glib_cv_stack_grows=no|\
--disable-shared --enable-static --disable-selinux --disable-fam \
    --disable-xattr --disable-largefile --enable-regex --enable-threads=no"

LIBXML2="libxml2-2.7.3|\
.tar.gz|\
ftp://xmlsoft.org/libxml2/|\
|\
--disable-shared --enable-static --with-minimum --with-push --with-sax1 \
    --with-tree"

MEANWHILE="v1.1.1|\
.tar.gz|\
https://github.com/obriencj/meanwhile/archive/|\
|\
--enable-static --disable-shared --disable-debug --disable-mailme \
    --disable-doxygen|\
meanwhile-1.1.1"

LIBGADU="libgadu-1.12.2|\
.tar.gz|\
https://github.com/wojtekka/libgadu/releases/download/1.12.2/|\
|\
--enable-static --disable-shared --with-c99-vsnprintf"

JSON_GLIB="json-glib-0.6.2|\
.tar.gz|\
http://folks.o-hand.com/~ebassi/sources/|\
autoreconf; |\
proto--enable-static --disable-shared"

PIDGIN_FACEBOOK="pidgin-facebookchat|\
-source-1.50.tar.bz2|\
http://pidgin-facebookchat.googlecode.com/files/|\
PLUGIN|\
facebook"

PIDGIN="pidgin-2.5.6|\
.tar.bz2|\
http://prdownloads.sourceforge.net/sourceforge/pidgin/|\
autoreconf; LDFLAGS=\"$LDFLAGS -ljson-glib-1.0\" |\
--disable-shared --enable-static --disable-gtkui --disable-consoleui \
    --disable-sm --disable-screensaver --disable-startup-notification \
    --disable-gtkspell --disable-gestures --disable-nm --disable-tcl \
    --disable-gstreamer --disable-tk --disable-avahi --disable-dbus \
    --disable-largefile --disable-perl --enable-nss=no --enable-gnutls=no \
    --enable-openssl=yes \
    --with-static-prpls=\"irc jabber msn myspace oscar yahoo facebook\""

LIBTAR="libtar-1.2.11|\
.tar.gz|\
ftp://ftp.feep.net/pub/software/libtar/|\
CFLAGS=\"-DHAVE_STDARG_H $CFLAGS\"
    compat_cv_func_makedev_three_args=no|\
--without-zlib"

XZ="xz-4.999.9beta|\
.tar.gz|\
http://tukaani.org/xz/|\
NATIVE|\
"


build()
{
    file=`echo $1 | cut -d\| -f1`
    ext=`echo $1 | cut -d\| -f2`
    url=`echo $1 | cut -d\| -f3`
    preconf=`echo $1 | cut -d\| -f4`
    postconf=`echo $1 | cut -d\| -f5`
    extractedfile=`echo $1 | cut -d\| -f6`

    if [ ! -f $file.stamp ]; then
        if [ ! -f $file$ext ]; then
            wget $url$file$ext
        fi
        if [ ! -d $file ]; then
            tar vxf $file$ext
            if [ ! -d $file ]; then
                cd $extractedfile
            else
                cd $file
            fi
            if [ -f ../../patches/$file.patch ]; then
                patch -p1 < ../../patches/$file.patch
            fi
        else
            cd $file
        fi
        if [ "$file" = "`echo $PIDGIN | cut -d\| -f1`" ]; then
            unpack_plugin "$PIDGIN_FACEBOOK"
        fi
        if [ ! "$preconf" = "PLUGIN" ]; then
            if [ -f 'autogen.sh' ]; then
                sh -c "$preconf ./autogen.sh $CROSS_BUILD $postconf"
            else
                sh -c "$preconf ./configure $CROSS_BUILD $postconf"
            fi
            make $JOBS
            make $JOBS install
        fi
        cd ..
        touch $file.stamp
    fi
}

clean()
{
    file=`echo $1 | cut -d\| -f1`
    rm -rf $file
    rm -f $file.stamp
}

unpack_plugin()
{
    plugin_file=`echo $1 | cut -d\| -f1`
    plugin_name=`echo $1 | cut -d\| -f5`
    cp ../$plugin_file/*.c libpurple/protocols/$plugin_name/
    cp ../$plugin_file/*.h libpurple/protocols/$plugin_name/
    cp ../$plugin_file/facebook16.png pidgin/pixmaps/protocols/16/$plugin_name.png
    cp ../$plugin_file/facebook22.png pidgin/pixmaps/protocols/22/$plugin_name.png
    cp ../$plugin_file/facebook48.png pidgin/pixmaps/protocols/48/$plugin_name.png
}


if [ "$1" = "clean" ]; then

    cd third_party || exit 0

#    clean "$BINUTILS"
#    clean "$GLIB"
    clean "$LIBXML2"
    clean "$MEANWHILE"
    clean "$LIBGADU"
    clean "$JSON_GLIB"
#    clean "$PIDGIN_FACEBOOK"
#    clean "$PIDGIN"
#    clean "$LIBTAR"

    clean "$XZ"
    rm -rf xz

    rm -rf `uname -m`

else

    set -e
    trap 'echo Build failed' EXIT

    mkdir -p third_party
    cd third_party

#    build "$BINUTILS"
#    build "$GLIB"
#    build "$LIBXML2"
#    build "$MEANWHILE"
    build "$LIBGADU"
    build "$JSON_GLIB"
#    build "$PIDGIN_FACEBOOK"
#    build "$PIDGIN"
#    build "$LIBTAR"

    # install pidgin pixmaps and sounds
    if [ ! -d $PREFIX/share/purple/pixmaps ]; then
        install -d $PREFIX/share/purple
        file=`echo $PIDGIN | cut -d\| -f1`
        cp -a $file/pidgin/pixmaps $PREFIX/share/purple
        cp -a $file/share/sounds $PREFIX/share/purple
    fi

    # native build
    build "$XZ"

    trap - EXIT
fi
