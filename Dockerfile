FROM fedora:28

RUN dnf -y -v install \
    autoconf \
    automake \
    bindfs \
    bison \
    clang \
    clang-analyzer \
    cmake \
    desktop-file-utils \
    elfutils-libelf-devel \
    findutils \
    fuse \
    gamin-devel \
    gcc \
    gcc-c++ \
    gettext \
    git \
    glibc-devel \
    glibc-headers \
    glibc-langpack-de \
    glibc-langpack-el \
    glibc-langpack-el \
    glibc-langpack-en \
    glibc-langpack-es \
    glibc-langpack-es \
    glibc-langpack-fa \
    glibc-langpack-fr \
    glibc-langpack-hr \
    glibc-langpack-ja \
    glibc-langpack-lt \
    glibc-langpack-pl \
    glibc-langpack-ru \
    glibc-langpack-tr \
    glib2\
    glib2-devel\
    gtk-doc \
    itstool \
    lcov \
    libattr-devel \
    libffi-devel \
    libmount-devel \
    libselinux-devel \
    libtool \
    libxslt \
    make \
    mercurial \
    ncurses-compat-libs \
    ninja-build \
    patch \
    pcre-devel \
    protobuf-c-compiler \
    python3 \
    python3-pip \
    python3-wheel \
    systemtap-sdt-devel \
    unzip \
    wget \
    xz \
    zlib-devel \
 && dnf clean all

WORKDIR /opt
ENV ANDROID_NDK_PATH /opt/android-ndk
COPY android-download-ndk.sh .
RUN ./android-download-ndk.sh
COPY android-setup-env.sh .
RUN ./android-setup-env.sh arm64 21
RUN ./android-setup-env.sh arm64 28
RUN rm -rf $ANDROID_NDK_PATH

RUN pip3 install meson==0.49.2

ARG HOST_USER_ID=5555
ENV HOST_USER_ID ${HOST_USER_ID}
RUN useradd -u $HOST_USER_ID -ms /bin/bash user

#USER user
WORkDIR /home/user

ENV LANG C.UTF-8

#Download dependencies

RUN wget -qO- https://github.com/obriencj/meanwhile/archive/v1.1.1.tar.gz | tar xzf -
RUN wget -qO- https://www.sqlite.org/2020/sqlite-autoconf-3340000.tar.gz | tar xzf -
RUN wget -qO- ftp://xmlsoft.org/libxml2/libxml2-2.7.3.tar.gz | tar xzf -
RUN wget -qO- https://github.com/wojtekka/libgadu/releases/download/1.12.2/libgadu-1.12.2.tar.gz | tar xzf -
RUN wget -qO- https://github.com/rockdaboot/libpsl/releases/download/0.21.1/libpsl-0.21.1.tar.gz | tar xzf -
RUN wget -qO- https://ftp.gnu.org/gnu/nettle/nettle-3.4.1.tar.gz | tar xzf -
RUN wget -qO- https://gmplib.org/download/gmp/gmp-6.2.1.tar.xz | tar xJf -
RUN wget -qO- https://www.gnupg.org/ftp/gcrypt/gnutls/v3.6/gnutls-3.6.15.tar.xz | tar xJf -
RUN wget -qO- https://download.gnome.org/sources/gmime/3.2/gmime-3.2.7.tar.xz | tar xJf -
RUN wget -qO- https://gstreamer.freedesktop.org/data/pkg/android/1.18.2/gstreamer-1.0-android-universal-1.18.2.tar.xz | tar xJf -
RUN wget -qO- ftp://alpha.gnu.org/pub/gnu/libidn/libidn-0.1.0.tar.gz | tar xzf -
RUN git clone https://gitlab.gnome.org/GNOME/glib.git
RUN git clone https://gitlab.gnome.org/GNOME/json-glib.git
RUN git clone https://gitlab.gnome.org/GNOME/libsoup.git
RUN git clone https://gitlab.freedesktop.org/libnice/libnice/
RUN hg clone https://keep.imfreedom.org/gplugin/gplugin
RUN hg clone https://keep.imfreedom.org/pidgin/pidgin/
RUN git clone https://gitlab.freedesktop.org/gstreamer/gst-build.git

#Copy some build scripts used by mulitple libraries

COPY build_meson.sh .
COPY build_autotools.sh .


RUN mkdir patches

#Build glib

RUN ./build_meson.sh arm64 28 glib '-Diconv=auto -Dinternal_pcre=true'
RUN ninja -C glib_build install

#Build json-glib

RUN pip3 install meson==0.53.0
RUN ./build_meson.sh arm64 28 json-glib '-Dintrospection=disabled -Dgtk_doc=disabled -Dman=false -Dtests=false'
RUN ninja -C json-glib_build 
RUN ninja -C json-glib_build install

#Build meanwhile

RUN ./build_autotools.sh arm64 28 "meanwhile-1.1.1|\
meanwhile|\
|\
--enable-static --disable-shared --disable-debug --disable-mailme --disable-doxygen"

#Build sqlite

RUN ./build_autotools.sh arm64 28 "sqlite-autoconf-3340000|\
sqlite3"

#Build libxml

RUN ./build_autotools.sh arm64 28 "libxml2-2.7.3|\
libxml-2.0|\
|\
--disable-shared --enable-static --with-minimum --with-push --with-sax1 --with-tree"

#Build libgadu

RUN ./build_autotools.sh arm64 28 "libgadu-1.12.2|\
libgadu|\
|\
--without-pthread --enable-static --disable-shared --with-c99-vsnprintf"

#Build libpsl

RUN ./build_autotools.sh arm64 28 "libpsl-0.21.1|\
libpsl"

#Build libsoup

COPY patches/libsoup.patch ./patches
RUN ./build_meson.sh arm64 28 libsoup '-Dgssapi=auto -Dntlm=auto -Dbrotli=auto -Dtls_check=false -Dintrospection=disabled -Dvapi=disabled -Dgtk_doc=false -Dtests=false -Dinstalled_tests=false -Dsysprof=disabled -Dfuzzing=disabled'
RUN ninja -C libsoup_build install

#Build libgmp

RUN ./build_autotools.sh arm64 28 "gmp-6.2.1|"

#Build nettle

RUN ./build_autotools.sh arm64 28 "nettle-3.4.1|"

#Build gnutls
COPY build_gnutls.sh .
RUN ./build_gnutls.sh arm64 28 "gnutls-3.6.15|\
|\
|\
--verbose --with-included-libtasn1 --with-included-unistring --without-p11-kit --with-gettext"
RUN ./build_meson.sh arm64 28 libnice '-Dgupnp=disabled -Dgstreamer=disabled -Dexamples=disabled -Dtests=disabled -Dgtk_doc=disabled -Dintrospection=disabled'
RUN ninja -C libnice_build install

#Zlib disabled for now
#RUN wget https://zlib.net/zlib-1.2.11.tar.gz | tar vxf
#COPY build_zlib.sh .
#RUN ./build_zlib.sh arm64 28 "zlib-1.2.11"

#Libiconv disabled for now
#RUN wget https://ftp.gnu.org/pub/gnu/libiconv/libiconv-1.16.tar.gz
#RUN ./build_autotools.sh arm64 28 "libiconv-1.16"

#Build gmime
COPY build_gmime.sh .
RUN ./build_gmime.sh arm64 28 "gmime-3.2.7|\
|\
export ac_cv_have_iconv_detect_h=no;"

#Build gplugin 

RUN ./build_meson.sh arm64 28 gplugin '-Ddoc=false -Dintrospection=false -Dgtk3=false -Dhelp2man=false -Dinstall-gplugin-gtk-viewer=false -Dinstall-gplugin-query=false -Dlua=false -Dnls=false -Dperl5=false -Dpython3=false -Dtcc=false -Dvapi=false'
RUN ninja -C gplugin_build install

#Build libidn


RUN ./build_autotools.sh arm64 28 "libidn-0.1.0|"

#Build gstreamer
RUN pip3 install meson==0.54.0
#TODO: Move this up to the top
RUN dnf -y -v install flex
RUN ./build_meson.sh arm64 28 gst-build '-Dpython=disabled -Dlibav=disabled -Dlibnice=disabled -Dbase=disabled -Dgood=disabled -Dbad=disabled -Dugly=disabled -Ddevtools=disabled -Dges=disabled -Drstp_server=disabled -Domx=disabled -Dvaapi=disabled -Dsharp=disabled -Drs=disabled -Dgst-examples=disabled -Dtls=disabled -Dqt5=disabled -Dtests=disabled -Dexamples=disabled -Dintrospection=disabled -Dnls=disabled -Dorc=disabled -Ddoc=disabled -Dgtk_doc=disabled'
RUN ninja -C gst-build_build install

#Build pidgin

COPY build_meson_temp.sh .
RUN chmod +x build_meson_temp.sh

COPY patches/pidgin.patch ./patches
RUN ./build_meson_temp.sh arm64 28 pidgin '-Ddoc=false -Dintrospection=false -Dnettle=enabled -Dnls=false -Dpixmaps-install=false -Dfarstream=disabled -Dgstreamer-video=disabled -Dkwallet=disabled -Dplugins=false -Dsecret-service=disabled -Dvv=disabled -Davahi=disabled -Dcyrus-sasl=disabled -Dkrb4=false -Dlibgadu=enabled -Dmeanwhile=enabled -Dsilc=disabled -Dconsole-logging=false -Dgtkui=false -Dunity-integration=disabled -Dconsoleui=false -Dx=false' || true
RUN cat /home/user/pidgin_build/meson-logs/meson-log.txt && false
RUN ninja -C pidgin_build

