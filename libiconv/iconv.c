#include "iconv.h"
#include <errno.h>
#include <unicode/ucnv.h>
#include <stdlib.h>

struct iconv_priv
{
  UConverter *toconv;
  UConverter *fromconv;
  UChar pivot[1024];
  UChar *inpivot;
  UChar *outpivot;
};

iconv_t iconv_open (const char *tocode, const char *fromcode)
{
  struct iconv_priv *priv;
  UErrorCode err = U_ZERO_ERROR;

  priv = calloc (1, sizeof (struct iconv_priv));
  if (!priv)
    return (iconv_t)-1;
  if (! (priv->toconv = ucnv_open (tocode, &err))) goto error;
  if (! (priv->fromconv = ucnv_open (fromcode, &err))) goto error;
  priv->inpivot = priv->pivot;
  priv->outpivot = priv->pivot;

  return (iconv_t)priv;

error:
  free (priv->toconv);
  free (priv->fromconv);
  free (priv);
  return (iconv_t)-1;
}

size_t iconv (iconv_t cd, char **inbuf, size_t *inbytesleft, char **outbuf,
        size_t *outbytesleft)
{
  struct iconv_priv *priv;
  UErrorCode err = U_ZERO_ERROR;

  priv = (struct iconv_priv *)cd;
  if (!priv)
    {
      errno = EINVAL;
      return -1;
    }
  if (inbuf && *inbuf)
    {
      char *orig_inbuf = *inbuf;
      ucnv_convertEx (priv->toconv, priv->fromconv, outbuf, *outbuf +
          *outbytesleft, (const char **)inbuf, *inbuf + *inbytesleft,
          priv->pivot, &priv->inpivot, &priv->outpivot, priv->pivot +
          sizeof (priv->pivot), FALSE, TRUE, &err);
      if (!U_SUCCESS(err))
        {
          if (err == U_BUFFER_OVERFLOW_ERROR)
            errno = E2BIG;
          else
            errno = EINVAL;
          return -1;
        }

      return (size_t)(*inbuf - orig_inbuf);
    }
  else if (!inbuf || !*inbuf)
    {
      /* TODO: Write shift sequence when *outbuf is valid.  */
      priv->inpivot = priv->pivot;
      priv->outpivot = priv->pivot;

      return 0;
    }

  errno = EINVAL;
  return (size_t)-1;
}

iconv_t iconv_close (iconv_t cd)
{
  struct iconv_priv *priv;

  priv = (struct iconv_priv *)cd;
  if (!priv)
    {
      errno = EINVAL;
      return -1;
    }
  ucnv_close (priv->toconv);
  ucnv_close (priv->fromconv);
  free (priv);

  return 0;
}
