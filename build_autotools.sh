#!/bin/sh

set -e

export arch=$1
export api=$2
toolchain_path=/opt/android-toolchain-$arch-$api
prefix_path=/opt/android-toolchain-$arch-$api

target_host=aarch64-linux-android
export AR=$target_host-ar
export AS=$target_host-clang
export CC=$target_host-clang
export CXX=$target_host-clang++
export LD=$target_host-ld
export STRIP=$target_host-strip
export PATH=$PATH:$toolchain_path/bin
export PREFIX=/opt/android-toolchain-$arch-$api/
export TARGET=aarch64-unknown-linux-gnu
CROSS_BUILD="--host=$TARGET --target=$TARGET --prefix=$PREFIX"
NATIVE_BUILD="--target=$TARGET --prefix=$PREFIX/native"


export LDFLAGS="-L$PREFIX/lib -Wl,-rpath-link=$PREFIX/lib -L/opt/android-toolchain-$arch-$api/lib64/"
export CC="$toolchain_path/bin/$CC"
export CXX="$toolchain_path/bin/$CXX"
export AR="$toolchain_path/bin/$AR"
export LD="$toolchain_path/bin/$LD"
export STRIP="$toolchain_path/bin/$STRIP"
export PKGCONFIG="$PKG_CONFIG"
export PKG_CONFIG_PATH='/opt/android-toolchain-${arch}-${api}/lib64/pkgconfig/;/opt/android-toolchain-${arch}-${api}/lib/pkgconfig/'

export GLIB_CFLAGS='-I/opt/android-toolchain-${arch}-${api}/include/glib-2.0/ -I/opt/android-toolchain-${arch}-${api}/lib64/glib-2.0/include/'
export CFLAGS="-I$PREFIX/include -I$DROID/external/icu4c/common -O2 -I/opt/android-toolchain-${arch}-${api}/include"
export GLIB_LIBS='-L/opt/android-toolchain-${arch}-${api}/lib64/glib-2.0/'
export NETTLE_CFLAGS='-I/opt/android-toolchain-${arch}-${api}/include/'
export NETTLE_LIBS='-L/opt/android-toolchain-${arch}-${api}/ -lnettle'
export HOGWEED_CFLAGS='-I/opt/android-toolchain-${arch}-${api}/include/'
export HOGWEED_LIBS='-L/opt/android-toolchain-${arch}-${api}/ -lhogweed -lgmp'

set -ex

ME="$3"

build()
{
    file=`echo $1 | cut -d\| -f1`
    libname=`echo $1 | cut -d\| -f2`
    preconf=`echo $1 | cut -d\| -f3`
    postconf=`echo $1 | cut -d\| -f4`
    if [ ! -f $file.stamp ]; then
        if [ ! -d $file ]; then
            cd $extractedfile
        else
            cd $file
        fi
        if [ -f patches/$file.patch ]; then
            patch -p1 < ../../patches/$file.patch
        fi
        if [ -f 'bootstrap' ]; then
            sh -c "$preconf ./bootstrap.sh $CROSS_BUILD $postconf"
        fi
        if [ -f 'autogen.sh' ]; then
            sh -c "$preconf ./autogen.sh $CROSS_BUILD $postconf"
        else
            sh -c "$preconf ./configure $CROSS_BUILD $postconf"
        fi
        make $JOBS
        make $JOBS install
        if [ -f $PREFIX/lib/pkgconfig/$libname.pc ] && [ ! -f $PREFIX/lib64/pkgconfig/$libname.pc ]; then
            mv $PREFIX/lib/pkgconfig/$libname.pc $PREFIX/lib64/pkgconfig/$libname.pc
            sed -i -e 's/\/lib/\/lib64/g' $PREFIX/lib64/pkgconfig/$libname.pc
        fi
        cd ..
        touch $file.stamp
    fi
}

clean()
{
    file=`echo $1 | cut -d\| -f1`
    rm -rf $file
    rm -f $file.stamp
}

#unpack_plugin()
#{
#    plugin_file=`echo $1 | cut -d\| -f1`
#    plugin_name=`echo $1 | cut -d\| -f5`
#    cp ../$plugin_file/*.c libpurple/protocols/$plugin_name/
#    cp ../$plugin_file/*.h libpurple/protocols/$plugin_name/
#4$    cp ../$plugin_file/facebook16.png pidgin/pixmaps/protocols/16/$plugin_name.png
#    cp ../$plugin_file/facebook22.png pidgin/pixmaps/protocols/22/$plugin_name.png
#    cp ../$plugin_file/facebook48.png pidgin/pixmaps/protocols/48/$plugin_name.png
#}


if [ "$1" = "clean" ]; then

    clean "$ME"
    rm -rf xz

    rm -rf `uname -m`

else

    set -e
    trap 'echo Build failed' EXIT

    build "$ME"

    # install pidgin pixmaps and sounds
#    if [ ! -d $PREFIX/share/purple/pixmaps ]; then
#        install -d $PREFIX/share/purple
#        file=`echo $PIDGIN | cut -d\| -f1`
#        cp -a $file/pidgin/pixmaps $PREFIX/share/purple
#        cp -a $file/share/sounds $PREFIX/share/purple
#    fi

    # native build
#    build "$XZ"

    trap - EXIT
fi
