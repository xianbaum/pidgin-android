package org.pidroid.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import org.pidroid.IPidroidService;
import org.pidroid.app.PidroidApp;
import org.pidroid.PidroidConstants;

public class AccountList extends Activity {
    /* package */ static final String TAG = "PidroidApp";
    private PidroidApp mApp;

    @Override
    protected void onCreate(Bundle state) {
        Log.d(TAG, "[AccountList.onCreate]");
        super.onCreate(state);

        mApp = PidroidApp.acquireApp();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(PidroidConstants.ACCOUNT_CONNECTED);
        intentFilter.addAction(PidroidConstants.ACCOUNT_DISCONNECTED);
        registerReceiver(mBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "[AccountList.onDestroy]");

        unregisterReceiver(mBroadcastReceiver);
        mApp.releaseRef();

        super.onDestroy();
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent == null) {
                Log.e(TAG, "[AccountList.mBroadcastReceiver.onReceive]: received null intent");
                return;
            }
            String action = intent.getAction();
            Log.d(TAG, "[AccountList.mBroadcastReceiver.onReceive]: " + action);

            if (action.equals(PidroidConstants.ACCOUNT_CONNECTED)) {
                // TODO
            } else if (action.equals(PidroidConstants.ACCOUNT_DISCONNECTED)) {
                // TODO
            }
        }
    };
}
