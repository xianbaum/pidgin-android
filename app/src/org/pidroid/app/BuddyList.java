package org.pidroid.app;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import org.pidroid.IPidroidService;
import org.pidroid.app.PidroidApp;
import org.pidroid.PidroidConstants;
import org.pidroid.Protocol;
import org.pidroid.R;

public class BuddyList extends Activity {
    /* package */ static final String TAG = "PidroidApp";
    private PidroidApp mApp;
    private int mPendingConnections;

    /* XXX: The service will broadcast account authentication failures. The statusbar listener
       should prompt about this and allow the user to click on the notification. */

    /* XXX: The service will maintain two lists of messages: old and new. As new messages come in,
       they will be added to the new queue, until getMessages (which returns both queues) is
       called. After this call, the new queue will be prepended to the old queue. */


    @Override
    protected void onCreate(Bundle state) {
        Log.d(TAG, "[BuddyList.onCreate]");
        super.onCreate(state);

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.buddy_list);

        mApp = PidroidApp.acquireApp();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(PidroidConstants.CREATE_PURPLE_SUCCEEDED);
        intentFilter.addAction(PidroidConstants.CREATE_PURPLE_FAILED);
        registerReceiver(mBroadcastReceiver, intentFilter);

        int result = mApp.connectService();
        if (result == PidroidApp.CONNECT_FAILED) {
            PidroidApp.showErrorDialog(BuddyList.this, R.string.pidroid_load_failed);
        } else if (result == PidroidApp.CONNECT_WAIT) {
            refreshProgressIcon();
        } else /* PidroidApp.CONNECT_OK */ {
            connectAccounts();
        }
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "[BuddyList.onDestroy]");

        unregisterReceiver(mBroadcastReceiver);
        mApp.releaseRef();

        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.buddy_list, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_signout:
                mApp.disconnectService();
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /* package */ synchronized void refreshProgressIcon() {
        Log.d(TAG, "[BuddyList.refreshProgressIcon]");
        if (mApp.getState() == PidroidApp.STATE_LOADING) {
            setProgressBarIndeterminateVisibility(true);
        } else if (mPendingConnections != 0) {
            setProgressBarIndeterminateVisibility(true);
        } else {
            setProgressBarIndeterminateVisibility(false);
        }
    }

    private void connectAccounts() {
        refreshProgressIcon();

        /* TODO: Connect to all auto-connectable accounts. If the account has a remembered
           password try to connect. If not, display a password dialog for each account. If
           authentication fails, the statusbar notifier will pick up on it. */
    }

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent == null) {
                Log.e(TAG, "[BuddyList.mBroadcastReceiver.onReceive]: received null intent");
                return;
            }
            String action = intent.getAction();
            Log.d(TAG, "[BuddyList.mBroadcastReceiver.onReceive]: " + action);

            if (action.equals(PidroidConstants.CREATE_PURPLE_SUCCEEDED)) {
                connectAccounts();
            } else if (action.equals(PidroidConstants.CREATE_PURPLE_FAILED)) {
                PidroidApp.showErrorDialog(BuddyList.this, R.string.pidroid_load_failed);
            }
        }
    };
}
