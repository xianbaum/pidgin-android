package org.pidroid.app;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

public class AccountSettings extends Activity {
    /* package */ static final String TAG = "PidroidApp";

    @Override
    protected void onCreate(Bundle state) {
        Log.d(TAG, "[AccountSettings.onCreate]");
        super.onCreate(state);
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "[AccountSettings.onDestroy]");

        super.onDestroy();
    }
}
