package org.pidroid.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import org.pidroid.IPidroidService;
import org.pidroid.PidroidConstants;

public class PidroidApp extends Application {
    /* package */ static final String TAG = "PidroidApp";
    /* package */ static final int STATE_UNLOADED = 1;
    /* package */ static final int STATE_LOADING = 2;
    /* package */ static final int STATE_LOADED = 3;

    /* package */ static final int CONNECT_OK = 1;
    /* package */ static final int CONNECT_FAILED = 2;
    /* package */ static final int CONNECT_WAIT = 3;

    private static PidroidApp sApp;
    private int mRefCount;
    /* package */ IPidroidService mPidroidService;
    /* package */ int mState = STATE_UNLOADED;

    @Override
    public void onCreate  () {
        super.onCreate();
        sApp = this;

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(PidroidConstants.CREATE_PURPLE_SUCCEEDED);
        intentFilter.addAction(PidroidConstants.CREATE_PURPLE_FAILED);
        intentFilter.addAction(PidroidConstants.PIDROID_SERVICE_STOPPED);
        registerReceiver(mBroadcastReceiver, intentFilter);
    }

    @Override
    public void onTerminate() {
        unregisterReceiver(mBroadcastReceiver);
        super.onTerminate();
    }

    /* package */ static PidroidApp acquireApp() {
        sApp.addRef();
        return sApp;
    }

    /* package */ synchronized void addRef() {
        mRefCount++;
    }

    /* package */ synchronized void releaseRef() {
        mRefCount--;
        if (mRefCount == 0) {
            disconnectService();
        }
    }

    /* package */ int getState() {
        return mState;
    }

    /* package */ void setState(int state) {
        mState = state;
    }

    /* package */ IPidroidService getService() {
        return mPidroidService;
    }

    /* package */ synchronized int connectService() {
        Log.d(TAG, "[PidroidApp.connectPidroidService]");
        if (mState == STATE_UNLOADED) {
            Log.d(TAG, "[PidroidApp.connectService]: about to bind");
            Intent serviceIntent = new Intent();
            serviceIntent.setComponent(PidroidConstants.SERVICE_COMPONENT);
            startService(serviceIntent);
            if (!bindService(serviceIntent, mPidroidServiceConn, Context.BIND_AUTO_CREATE)) {
                return CONNECT_FAILED;
            }
            mState = STATE_LOADING;
        }

        if (mState == STATE_LOADING) {
            return CONNECT_WAIT;
        } else  /* STATE_LOADED */ {
            return CONNECT_OK;
        }
    }

    /* package */ synchronized void disconnectService() {
        Log.d(TAG, "[PidroidApp.disconnectPidroidService]");
        if (mState != STATE_UNLOADED) {
            Log.d(TAG, "[PidroidApp.disconnectService]: unbinding");
            unbindService(mPidroidServiceConn);
            mState = STATE_UNLOADED;
        }
    }

    /* package */ static void showErrorDialog(final Activity activity, int message) {
        new AlertDialog.Builder(activity)
            .setMessage(message)
            .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    activity.finish();
                }})
            .setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    activity.finish();
                }})
            .show();
    }

    private ServiceConnection mPidroidServiceConn = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            Log.d(TAG, "[PidroidApp.mPidroidServiceConn.onServiceConnected]");
            mPidroidService = IPidroidService.Stub.asInterface(service);
        }

        public void onServiceDisconnected(ComponentName className) {
            Log.d(TAG, "[PidroidApp.mPidroidServiceConn.onServiceDisconnected]");
        }
    };

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            if (intent == null) {
                Log.e(TAG, "[PidroidApp.mBroadcastReceiver.onReceive]: received null intent");
                return;
            }
            String action = intent.getAction();
            Log.d(TAG, "[PidroidApp.mBroadcastReceiver.onReceive]: " + action);

            if (action.equals(PidroidConstants.CREATE_PURPLE_SUCCEEDED)) {
                setState(STATE_LOADED);
            } else if (action.equals(PidroidConstants.CREATE_PURPLE_FAILED)) {
                setState(STATE_LOADED);
            } else if (action.equals(PidroidConstants.PIDROID_SERVICE_STOPPED)) {
                setState(STATE_UNLOADED);
            }
        }
    };
}
