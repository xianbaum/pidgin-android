package org.pidroid;

import org.pidroid.Protocol;

interface IPidroidService {
    /* Note: The return should be considered immutable. */
    Protocol[] getProtocols();
}
