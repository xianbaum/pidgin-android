package org.pidroid.service;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.util.Log;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import org.pidroid.IPidroidService;
import org.pidroid.PidroidConstants;
import org.pidroid.Protocol;

public class PidroidService extends Service {
    /* package */ static final String TAG = "PidroidService";
    private static final String ASSETS_DIR = "assets";
    private static final String LIBTARXZ = "libtarxz.so";
    private static final String TARXZ = "pidroid.tar.xz";
    private static final String LIBPIDROID = "/lib/libpidroid.so";
    private static final String VERSION_FILE = "/version.dat";
    // Note: this can only go up to 255
    private static final int VERSION = 1; // TODO: grab this from the build environment

    /* package */ Protocol[] mProtocols;
    private boolean mPidroidLoaded;

    private native boolean createPurple();
    private native boolean destroyPurple();
    private static native boolean extractAsset(String sourceDir, String assetName, String assetsDir);

    // TODO: use setForeground

    @Override
    public void onCreate() {
        super.onCreate();

        OnCreateThread onCreateThread = new OnCreateThread();
        onCreateThread.start();
    }

    private class OnCreateThread extends Thread {
        public void run() {
            doOnCreate();
        }
    }

    /* package */ void doOnCreate() {
        String assetsDir = getDir(ASSETS_DIR, Context.MODE_PRIVATE).getPath();
        if (!loadPidroid(assetsDir)) {
            stopSelfAndBroadcastError();
            return;
        }
        if (!createPurple()) {
            stopSelf();
            return;
        }
    }

    @Override
    public void onDestroy() {
        if (mPidroidLoaded) {
            destroyPurple();
        }
        // TODO: unload libpidroid
        Intent intent = new Intent(PidroidConstants.PIDROID_SERVICE_STOPPED);
        sendBroadcast(intent, PidroidConstants.REQUIRED_PERMISSION);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "[onBind]");
        return mBinder;
    }

    @Override
    public boolean onUnbind (Intent intent) {
        Log.d(TAG, "[onUnbind]");
        // if (mConnections.count() == 0) { // TODO
            stopSelf();
        // }
        return false;
    }

    private boolean loadPidroid(String assetsDir) {

        int version = -1; // getInstalledVersion(assetsDir); // TODO
        if (version != VERSION) {
            if (!installAssets(assetsDir)) {
                return false;
            }
        }

        try {
            Log.e(TAG, "[loadPidroid]: loading libpidroid.so");
            System.load(assetsDir + LIBPIDROID);
        } catch (UnsatisfiedLinkError e) {
            Log.e(TAG, "[loadPidroid]: failed to load libpidroid.so");

            // If we fail to load libpidroid.so force a reinstall next time we load
            new File(assetsDir + VERSION_FILE).delete();
            return false;
        }

        mPidroidLoaded = true;
        return true;
    }

    private String getSourceDir() {
        ApplicationInfo appInfo;
        try {
            appInfo = getPackageManager().getApplicationInfo("org.pidroid",
                PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            stopSelfAndBroadcastError();
            return null;
        }
        return appInfo.sourceDir;
    }

    private int getInstalledVersion(String assetsDir) {
        try {
            FileInputStream reader = new FileInputStream(assetsDir + VERSION_FILE);
            return reader.read();
        } catch (IOException e) {
            return -1;
        } catch (NumberFormatException e) {
            return -1;
        }
    }

    private boolean installAssets(String assetsDir) {
        Log.e(TAG, "[installAssets]");

        // It's bad if removeAssets fails, but not fatal
        removeAssets(assetsDir);

        String libtarxzPath = assetsDir + "/" + LIBTARXZ;
        try {
            new File(libtarxzPath).delete();
            FileOutputStream writer = new FileOutputStream(libtarxzPath);
            InputStream reader = getResources().getAssets().open(LIBTARXZ);
            byte[] buffer = new byte[65536];
            int bytesRead = reader.read(buffer);
            while (bytesRead > 0) {
                writer.write(buffer, 0, bytesRead);
                bytesRead = reader.read(buffer);
            }
            writer.close();
        } catch (IOException e2) {
            Log.e(TAG, "[installAssets]: failed to extract libtarxz.so");
            return false;
        }

        try {
            System.load(libtarxzPath);
        } catch (UnsatisfiedLinkError e2) {
            Log.e(TAG, "[installAssets]: failed to load libtarxz.so");
            return false;
        }

        String sourceDir = getSourceDir();
        if (sourceDir == null) {
            return false;
        }
        try {
            // TODO: recursively delete everything in sourceDir
            // TODO: remove this hack
            File libpidroid = new File(assetsDir + LIBPIDROID);
            libpidroid.delete();

            if (!extractAsset(sourceDir, TARXZ, assetsDir)) {
                return false;
            }
        } catch (UnsatisfiedLinkError e) {
            Log.e(TAG, "[installAssets]: extractAsset not set");
            return false;
        }
        try {
            FileOutputStream writer = new FileOutputStream(assetsDir + VERSION_FILE);
            writer.write(VERSION);
            writer.close();
        } catch (IOException e) {
            Log.e(TAG, "[installAssets]: failed to write version");
            return false;
        }

        return true;
    }

    private boolean removeAssets(String assetsDir) {
        // TODO
        return true;
    }

    private void stopSelfAndBroadcastError() {
        Intent intent = new Intent(PidroidConstants.CREATE_PURPLE_FAILED);
        sendBroadcast(intent, PidroidConstants.REQUIRED_PERMISSION);
        stopSelf();
    }


    //
    // IPidroidService implementation
    //
    private final IPidroidService.Stub mBinder = new IPidroidService.Stub() {
        public Protocol[] getProtocols() {
            return mProtocols;
        }
    };
}
