package org.pidroid;

import android.os.Parcel;
import android.os.Parcelable;

public class Protocol implements Parcelable {

    public String mName;
    public String mId;

    public Protocol(String name, String id) {
        mName = name;
        mId = id;
    }

    public Protocol(Parcel source) {
        // TODO
    }

    public void writeToParcel(Parcel dest, int flags) {
        // TODO
    }

    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<Protocol> CREATOR = new Parcelable.Creator<Protocol>() {
        public Protocol createFromParcel(Parcel source) {
            return new Protocol(source);
        }

        public Protocol[] newArray(int size) {
            return new Protocol[size];
        }
    };
}
