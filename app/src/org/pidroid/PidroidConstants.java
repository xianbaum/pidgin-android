package org.pidroid;

import android.content.ComponentName;

public class PidroidConstants {
    public static final ComponentName SERVICE_COMPONENT = new ComponentName(
            "org.pidroid", "org.pidroid.service.PidroidService");

    // Keep these in sync with libpidroid/jni.cpp
    public static final String REQUIRED_PERMISSION =
            "org.pidroid.permission.PIDROID_SERVICE";
    public static final String CREATE_PURPLE_SUCCEEDED =
            "org.pidroid.intent.action.CREATE_PURPLE_SUCCEEDED";
    public static final String CREATE_PURPLE_FAILED =
            "org.pidroid.intent.action.CREATE_PURPLE_FAILED";
    public static final String PIDROID_SERVICE_STOPPED =
            "org.pidroid.intent.action.PIDROID_SERVICE_STOPPED";
    public static final String ACCOUNT_CONNECTED =
            "org.pidroid.intent.action.ACCOUNT_CONNECTED";
    public static final String ACCOUNT_DISCONNECTED =
            "org.pidroid.intent.action.ACCOUNT_DISCONNECTED";
}
