arch=$1
api=$2
toolchain_path=/opt/android-toolchain-$arch-$api
prefix_path=/opt/android-$arch-$api
target_host=aarch64-linux-android
#export AR=$target_host-ar
#export AS=$target_host-clang
#export CC=$target_host-clang
#export CXX=$target_host-clang++
#export LD=$target_host-ld
#export STRIP=$target_host-strip
#export PATH=$PATH:$toolchain_path/bin
export PREFIX=/opt/android-toolchain-arm64-28/
#export TARGET=aarch64-unknown-linux-gnu
CROSS_BUILD="--cross-file=/opt/cross_file_android_arm64_28.txt"
#export CFLAGS="-I$PREFIX/include -I$DROID/external/icu4c/common -O2 -I/home/user/androidlib/include"

#export LDFLAGS="-L$PREFIX/lib -Wl,-rpath-link=$PREFIX/lib -L/home/user/androidlib/lib64/"
#export CC="$toolchain_path/bin/$CC"
#export CXX="$toolchain_path/bin/$CXX"
#export AR="$toolchain_path/bin/$AR"
#export LD="$toolchain_path/bin/$LD"
#export STRIP="$toolchain_path/bin/$STRIP"
export PKGCONFIG="$PKG_CONFIG"
export PKG_CONFIG_PATH='/opt/android-toolchain-arm64-28/lib64/pkgconfig/:/opt/android-toolchain-arm64-28/lib/pkgconfig/'
#export GLIB_CFLAGS='-I/opt/android-toolchain-arm64-28/glib-2.0 -I/opt/android-toolchain-arm64-28/lib64/glib-2.0/include/'
#export GLIB_LIBS='-L/opt/android-toolchain-arm64-28/lib64/glib-2.0'
set -ex

if [ -f patches/$3.patch ]; then
    cd $3
    patch -p0 < ../patches/$3.patch
    cd ..
fi

sh -c "meson $CROSS_BUILD $4 $3 $3_build --prefix=$PREFIX"

#trap 'echo Build failed' EXIT
