#include "libintl.h"
#include <stddef.h>
#include <string.h>
#include <errno.h>

// TODO

const char * ngettext (const char * msgid, const char * msgid_plural,
        unsigned long int n)
{
  return (n == 1) ? msgid : msgid_plural;
}

const char * dngettext (const char * domainname, const char * msgid,
        const char * msgid_plural, unsigned long int n)
{
  return (n == 1) ? msgid : msgid_plural;
}

const char * dcngettext (const char * domainname, const char * msgid,
        const char * msgid_plural, unsigned long int n, int category)
{
  return (n == 1) ? msgid : msgid_plural;
}

const char * gettext (const char * msgid)
{
  return msgid;
}

const char * dgettext (const char * domainname, const char * msgid)
{
  return msgid;
}

const char * dcgettext (const char * domainname, const char * msgid,
        int category)
{
  return msgid;
}

const char * textdomain (const char * domainname)
{
  if (!domainname)
    return "messages";
  return domainname;
}

const char * bindtextdomain (const char * domainname, const char * dirname)
{
  return domainname;
}
