#ifndef _LIBINTL_H
#define _LIBINTL_H

const char * ngettext (const char *, const char *, unsigned long int);
const char * dngettext (const char *, const char *, const char *,
    unsigned long int);
const char * dcngettext (const char *, const char *, const char *,
    unsigned long int, int);

const char * gettext (const char *);
const char * dgettext (const char *, const char *);
const char * dcgettext (const char *, const char *, int);

const char * textdomain (const char *);
const char * bindtextdomain (const char *, const char *);

#endif /* _LIBINTL_H */
